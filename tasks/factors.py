def factors(n):
    the_factors = []
    for i in range (2, n):
        if n % i == 0:
            the_factors.append(i)

    return the_factors


print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
