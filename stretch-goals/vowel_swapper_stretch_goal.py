def vowel_swapper(string):

    for char in string:
        if char.count("a") == 1:
            string.replace("a", "4", 2)

    return string

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console

#Need to replace 2nd occurrence of a character in a string
    #Needs to be case insensitive

#string = string.replace("a", "4", )
#string = string.replace("e", "3")
#string = string.replace("E", "3")
#string = string.replace("i", "!")
#string = string.replace("I", "!")
#string = string.replace("o", "ooo")
#string = string.replace("O", "OOO")
#string = string.replace("u", "|_|")
#string = string.replace("U", "|_|")